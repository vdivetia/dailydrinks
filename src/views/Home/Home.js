import List from "@/components/Order/List/List.vue";

export default {
  name: "Home",
  components: {
    List
  },
  data() {
    return {
      orderList: []
    };
  },
  computed: {},
  mounted() {},
  methods: {
    /**
     * Add new blank order entry in the order table
     */
    async addOrder() {
      const orderLength = this.orderList.length - 1;
      // if length > -1 then check name and price if both empty  then dont allow to add order
      if (orderLength > -1) {
        const { name, price } = this.orderList[orderLength];

        if (!name && !price) {
          return false;
        }
      }

      this.orderList.push({
        type: "input",
        name: null,
        price: null,
        note: null,
        isEdit: false,
        oldValue: {
          name: "",
          price: "",
          note: ""
        },
        error: {
          name: "",
          price: ""
        }
      });
    }
  }
};
