export default {
  name: "List",
  props: ["orderList"],
  data() {
    return {};
  },
  computed: {
    validateName: function() {
      return;
    }
  },
  mounted() {},
  methods: {
    /**
     * Validating user input
     * @param evt
     */
    isNumber: function(evt) {
      evt = evt || window.event;
      let charCode = evt.which ? evt.which : evt.keyCode;
      if (charCode === 46 || (charCode > 47 && charCode < 58)) {
        // valid input 1-9 and '.' allowed
      } else {
        evt.preventDefault();
      }
    },
    /**
     * Perform save operation
     * @param rowIndex
     */
    handleSave(rowIndex) {
      let errorFlag = false;

      const updatedData = [...this.orderList];

      // Validating Name Input
      if (!updatedData[rowIndex].name) {
        updatedData[rowIndex].error.name = "Name is required";
        errorFlag = true;
      } else {
        updatedData[rowIndex].error.name = "";
      }

      // Validating Price Input
      if (!updatedData[rowIndex].price) {
        updatedData[rowIndex].error.price = "Price is required";
        errorFlag = true;
      } else {
        updatedData[rowIndex].error.price = "";
      }

      // NOTE if all looks good then update type
      if (!errorFlag) {
        updatedData[rowIndex].type = "data";
        updatedData[rowIndex].isEdit = true;
        this.$emit("update:orderList", updatedData);
      }
    },
    /**
     * Perform edit operation on orderlist
     * @param rowIndex
     */
    handleEdit(rowIndex) {
      this.isEdit = true;
      const { name, price, note } = this.orderList[rowIndex];
      // on edit store old value
      this.orderList[rowIndex].oldValue = {
        name,
        price,
        note
      };
      // clear error on edit
      this.orderList[rowIndex].error = {
        name: "",
        price: ""
      };
      // change input type
      this.orderList[rowIndex].type = "input";
    },
    /**
     * Perform delete operation by index
     * @param rowIndex
     */
    handleDelete(rowIndex) {
      // Using default javascript confirmation
      if (!confirm("Do you want to delete selected record?")) {
        return false;
      }

      const updatedData = [...this.orderList];
      updatedData.splice(rowIndex, 1);

      // update parent orderList data
      this.$emit("update:orderList", updatedData);
    },
    /**
     * Return back to previous state
     * @param rowIndex
     */
    handleCancel(rowIndex) {
      // if newly added element then delete last one
      const updatedOrderList = [...this.orderList];
      updatedOrderList[rowIndex].type = "data";

      if (!updatedOrderList[rowIndex].isEdit) {
        updatedOrderList.splice(rowIndex, 1);
      } else {
        let { name, price, note } = updatedOrderList[rowIndex].oldValue;
        updatedOrderList[rowIndex].name = name;
        updatedOrderList[rowIndex].price = price;
        updatedOrderList[rowIndex].note = note;
      }

      this.$emit("update:orderList", updatedOrderList);
    }
  }
};
