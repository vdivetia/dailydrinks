# DailyDrinks

## Project setup

### PreRequested

- Node.js 12.x (you can also use nvm <a href="https://github.com/nvm-sh/nvm" target="_blank">Click Here</a>)
- Yarn (optional <a href="https://classic.yarnpkg.com/en/docs/install/#debian-stable" target="_blank">Click Here</a>)

```
npm install
```

OR

```
yarn install
```

### Run development environment

```
npm run serve
```

OR

```
yarn serve
```
